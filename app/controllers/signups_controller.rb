class SignupsController < ApplicationController
  http_basic_authenticate_with name: "Dk'^51/vg8Uhy2T", password: "g5GMzzNYXH>v03C", only: :index

  def create
    @signup = Signup.create(signup_params)

    respond_to do |format|
      format.html {}
      format.js { render json: @signup }
    end
  end

  def dup
    respond_to do |format|
      if Signup.where(username: params[:username]).empty?
        format.js { render json: { status: 'OK' } }
      else
        format.js { render json: { status: 'DUP' }, status: :unprocessable_entity }
      end
    end
  end

  def index
    @signups = Signup.all
    respond_to do |format|
      format.csv { send_data @signups.to_csv }
    end
  end

  private
    def signup_params
      params.require(:signup).permit(:email, :username)
    end
end
